
defaultRoles = ['super-admin','admin','manager','user']

initRoles = (roles) ->
  defaultHierarchy = false
  unless roles
    defaultHierarchy = true
    roles = defaultRoles

  roles.forEach((role) ->
    Roles.createRole(role, {unlessExists: true}))

  if defaultHierarchy
    Roles.addRolesToParent('admin', "super-admin")
    Roles.addRolesToParent('manager','admin')
    Roles.addRolesToParent('user','manager')

class MeteorProfileClass
  constructor: (@teamName, roles) ->
    initRoles(roles)
    share.initMethods(@teamName,roles)
    share.teamName = @teamName
    @Schemas = share.Schemas
    @ProfilePictures = share.ProfilePictures
    @Methods = share.Methods
