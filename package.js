Package.describe({
  name: 'abate:meteor-user-profiles',
  version: '0.0.1',
  summary: 'Users With profile and signup templates',
  git: '',
  documentation: 'README.md',
})

Package.onUse((api) => {
  api.versionsFrom('1.4')
  api.use([
    'mongo',
    'coffeescript',
    'ecmascript',
    'tmeasday:check-npm-versions',
    'spacebars',
    'check',
    'aldeed:collection2',
    'aldeed:autoform@6.0.0',

    'dburles:mongo-collection-instances',
    'ostrio:autoform-files',
    'ostrio:files',

    'piemonkey:roles',
    'abate:autoform-components',
    'jss:flag-icon',

    'universe:i18n',
    'universe:i18n-blaze',
  ], ['client', 'server'])

  api.use('templating', 'client')
  api.use('session', 'client')

  api.add_files([
    'both/collections/pictures.coffee',
    'both/collections/users.coffee',
    'both/methods.coffee',
    'both/methods.js',
    'api.coffee',
  ], ['server', 'client'])

  api.add_files([
    'global_helpers.coffee',
    // "frontend/user.coffee",
    // "frontend/user.html",
    'client/css/flags.css',
    'client/backend/users.html',
    'client/backend/users.coffee',
    'client/frontend/users.html',
    'client/frontend/users.coffee',
    'client/frontend/edit-emails.html',
    'client/frontend/edit-emails.js',
  ], 'client')

  api.add_files([
    'server/init.js',
  ], 'server')

  api.add_files(['i18n/en.i18n.json'], ['client', 'server'])

  api.export(['MeteorProfileClass'])
})

// Package.onTest(function(api) {
// api.use('ecmascript');
// api.use('tinytest');
// api.use('i18n-inline');
// api.mainModule('i18n-inline-tests.js');
// });
