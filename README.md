
===Meteor User Profiles

This module provide a template with the list of users and a set of operations
restricted via the Roles module.

This module does not publish any data about users.
