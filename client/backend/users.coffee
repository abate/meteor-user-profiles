import SimpleSchema from 'simpl-schema'
SimpleSchema.extendOptions(['autoform'])

# this template does not do any subscriptions. It should be called
# with all the subscriptions already in place.
#Template.allUsersProfile.onCreated () ->
#  template = this

# TODO: this will breake if this module is used with multiple team names
share.teamName = ""

Template.allUsersProfile.bindI18nNamespace("abate:meteor-user-profiles")
Template.allUsersProfile.helpers
  'displayRoles': (userId) ->
    if Roles.userIsInRole(userId, [ 'super-admin' ], share.teamName)
      "super-admin"
    else if Roles.userIsInRole(userId, [ 'admin' ], share.teamName)
      "admin"
    else if Roles.userIsInRole(userId, [ 'manager' ], share.teamName)
      "manager"
    else if Roles.userIsInRole(userId, [ 'user' ], share.teamName)
      "user"

Template.usersProfileActions.bindI18nNamespace("abate:meteor-user-profiles")
Template.removeUserAction.bindI18nNamespace("abate:meteor-user-profiles")
Template.makeUserUserAction.bindI18nNamespace("abate:meteor-user-profiles")
Template.makeAdminUserAction.bindI18nNamespace("abate:meteor-user-profiles")
Template.makeManageUserAction.bindI18nNamespace("abate:meteor-user-profiles")
Template.banUserAction.bindI18nNamespace("abate:meteor-user-profiles")
Template.unBanUserAction.bindI18nNamespace("abate:meteor-user-profiles")
Template.changePwdUserAction.bindI18nNamespace("abate:meteor-user-profiles")
Template.adminChangeUserPassword.bindI18nNamespace("abate:meteor-user-profiles")
Template.editProfileUserAction.bindI18nNamespace("abate:meteor-user-profiles")
Template.editEmailUserAction.bindI18nNamespace("abate:meteor-user-profiles")
Template.enrollUserAction.bindI18nNamespace("abate:meteor-user-profiles")

Template.removeUserAction.events
  'click [data-action="remove-user"]': (event, template) ->
    { callback, user } = template.data
    Meteor.call 'Accounts.removeUser', user._id, (err,res) ->
      if callback?
        callback(err,res)

Template.enrollUserAction.events
  'click [data-action="send-invitation"]': (event, template) ->
    { callback, user } = template.data
    Meteor.call 'Accounts.sendEnrollment', user._id , (err,res) ->
      if callback?
        callback(err,res)

Template.changePwdUserAction.events
  'click [data-action="change-password"]': (event, template) ->
    schema = new SimpleSchema({
      userId:
        type: String
        autoform:
          type: "hidden"
      password:
        type: String
        autoform:
          type: "password"
          label: () -> i18n.__("abate:meteor-user-profiles", "password")
      password_again:
        type: String
        autoform:
          type: "password"
          label: () -> i18n.__("abate:meteor-user-profiles", "password_again")
    })
    { callback, user } = template.data
    AutoFormComponents.ModalShowWithTemplate(
      'adminChangeUserPassword',
      { form: {
        schema: schema,
        insert: {
          id: "AdminChangeUserPassword",
          method: "Accounts.adminChangeUserPassword",
          label: () -> i18n.__("abate:meteor-user-profiles","change_password")
        },
        }, data: { userId : user._id }
      }, 'User Form', 'lg',
    )

Template.editProfileUserAction.events
  'click [data-action="edit-profile"]': (event, template) ->
    { callback, user } = template.data
    # XXX I think this can be a bit shorter. Refactor !
    ProfileSchema = new SimpleSchema(share.Schemas.Profile)
    ProfileSchema.extend({
      terms: {
        type: Boolean
        autoform:
          omit: false
        }
    })
    userSchema = new SimpleSchema({
        profile: {
          type: ProfileSchema,
          optional: true,
          },
    })
    AutoFormComponents.ModalShowWithTemplate(
      'insertUpdateTemplate',
      { form: {
        schema: userSchema,
        # collection: Meteor.users
        update: {
          id: "AdminUpdateUserProfile",
          method: "Accounts.UpdateUser",
          label: () -> i18n.__("abate:meteor-user-profiles","update_profile")
        },
        }, data: _.pick(user,['_id','profile'])
      }, 'Edit User Profile'
    )

Template.editEmailUserAction.events
  'click [data-action="edit-email"]': (event, template) ->
    { callback, user } = template.data
    AutoFormComponents.ModalShowWithTemplate(
      'editEmails', user,
      'Edit User Email'
    )

AutoForm.addHooks([
  'AdminChangeUserPassword',
  'AdminUpdateUserProfile'
],
  onSuccess: () ->
    AutoFormComponents.modalHide()
  )

Template.makeAdminUserAction.events
  'click [data-action="admin-user"]': (event, template) ->
    { callback, user } = template.data
    Meteor.call "Accounts.changeUserRole", user._id, "admin"

Template.makeManageUserAction.events
  'click [data-action="manager-user"]': (event, template) ->
    { callback, user } = template.data
    Meteor.call "Accounts.changeUserRole", user._id, "manager"

Template.makeUserUserAction.events
  'click [data-action="normal-user"]': (event, template) ->
    { callback, user } = template.data
    Meteor.call "Accounts.changeUserRole", user._id, "user"
