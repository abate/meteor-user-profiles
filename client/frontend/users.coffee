
Template.userProfile.onCreated () ->
  template = this
  template.subscribe('meteor-user-profiles.ProfilePictures', Meteor.userId())

Template.userProfile.helpers
  form: () ->
    collection: Meteor.users
    fields: "profile"
    update: { method: "Accounts.UpdateUser" }
  data: () -> Meteor.user()
