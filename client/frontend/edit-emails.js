Template.editEmails.bindI18nNamespace('abate:meteor-user-profiles')

Template.editEmails.onCreated(function onCreated() {
  const template = this
  template.user = new ReactiveVar(Meteor.user())
  template.errors = new ReactiveVar()
  if (template.data) {
    template.user.set(template.data)
  }
})

Template.editEmails.helpers({
  isPrimaryEmail(address) {
    const user = Template.instance().user.get()
    return (address === user.emails[0].address)
  },
  user() { return Template.instance().user.get() },
  errors() { return Template.instance().errors.get() },
})

Template.editEmails.events({
  'submit form': (event, template) => {
    event.preventDefault()

    const address = event.target.addEmail.value

    if (!_.isEmpty(address)) {
      /** Call method to add a new email and send the email address */
      const userId = template.user.get()._id
      Meteor.call('Accounts.addEmail', { userId, address }, (err) => {
        if (err) {
          console.log(`Error while adding new email address ${err}`)
          template.errors.set('Error while adding new email address: Incorrect or duplicate email address')
        } else {
          template.errors.set()
          template.user.set(Meteor.users.findOne(userId))
          event.target.addEmail.value = ''
        }
      })
    }
  },
  /** Send Verification Link */
  'click button[data-action="sendVerification"]': (event, template) => {
    const address = template.$(event.currentTarget).data('address')
    const userId = template.user.get()._id
    Meteor.call('Accounts.sendVerificationEmail', { userId, address }, (err) => {
      if (err) {
        console(`Error while sending verification email ${err}`)
        template.errors.set('Error while sending verification email')
      } else {
        template.errors.set()
        template.user.set(Meteor.users.findOne(userId))
      }
    })
  },
  /** Make email primary */
  'click button[data-action="makePrimary"]': (event, template) => {
    const address = template.$(event.currentTarget).data('address')
    const userId = template.user.get()._id
    Meteor.call('Accounts.makePrimary', { userId, address }, (err) => {
      if (err) {
        template.errors.set(`Error while setting primary email: ${err}`)
      } else {
        template.errors.set()
        template.user.set(Meteor.users.findOne(userId))
      }
    })
  },
  /** Remove email */
  'click button[data-action="removeEmail"]': (event, template) => {
    const address = template.$(event.currentTarget).data('address')
    const userId = template.user.get()._id
    Meteor.call('Accounts.removeEmail', { userId, address }, (err) => {
      if (err) {
        template.errors.set(`Error while removing email: ${err}`)
      } else {
        template.errors.set()
        template.user.set(Meteor.users.findOne(userId))
      }
    })
  },
})
