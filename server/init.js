Meteor.startup(() => {
  Meteor.users._ensureIndex({
    'profile.firstName': 1,
    'profile.lastName': 1,
    'emails.0.address': 1,
  })
})
